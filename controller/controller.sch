EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:sgen2-cache
LIBS:s_gen
LIBS:controller-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7400 7500 0    60   ~ 0
Solid State Generator Controller
Text Notes 10600 7650 0    60   ~ 0
1
Text Notes 7000 6650 0    60   ~ 0
Designed by:
Text Notes 7150 6750 0    60   ~ 0
Indrek Sünter
Text Notes 8150 7650 0    60   ~ 0
01.05.2016
$Comp
L MCP3004-RESCUE-controller U3
U 1 1 56488338
P 9700 2400
F 0 "U3" H 9500 1950 60  0000 C CNN
F 1 "MCP3004" H 9650 2850 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-14_4.4x5mm_Pitch0.65mm" H 9750 2150 60  0001 C CNN
F 3 "" H 9750 2150 60  0000 C CNN
	1    9700 2400
	1    0    0    -1  
$EndComp
$Comp
L CP C3
U 1 1 56488C4E
P 8800 1250
F 0 "C3" H 8825 1350 50  0000 L CNN
F 1 "10uF" H 8825 1150 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeC_EIA-6032_HandSoldering" H 8838 1100 30  0001 C CNN
F 3 "" H 8800 1250 60  0000 C CNN
	1    8800 1250
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 56488D12
P 9150 1250
F 0 "C4" H 9175 1350 50  0000 L CNN
F 1 "10nF" H 9175 1150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9188 1100 30  0001 C CNN
F 3 "" H 9150 1250 60  0000 C CNN
	1    9150 1250
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 56488F9F
P 10350 1250
F 0 "C5" H 10375 1350 50  0000 L CNN
F 1 "10nF" H 10375 1150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 10388 1100 30  0001 C CNN
F 3 "" H 10350 1250 60  0000 C CNN
	1    10350 1250
	1    0    0    -1  
$EndComp
Text Label 10950 1050 2    60   ~ 0
3V_Ref
$Comp
L +3.3V #PWR01
U 1 1 56489688
P 8800 950
F 0 "#PWR01" H 8800 800 50  0001 C CNN
F 1 "+3.3V" H 8800 1090 50  0000 C CNN
F 2 "" H 8800 950 60  0000 C CNN
F 3 "" H 8800 950 60  0000 C CNN
	1    8800 950 
	1    0    0    -1  
$EndComp
Text Notes 9150 700  0    60   ~ 0
Voltage reference for ADCs.
$Comp
L C C7
U 1 1 56489F55
P 10900 2300
F 0 "C7" H 10925 2400 50  0000 L CNN
F 1 "100nF" H 10925 2200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 10938 2150 30  0001 C CNN
F 3 "" H 10900 2300 60  0000 C CNN
	1    10900 2300
	1    0    0    -1  
$EndComp
Text Label 10450 2400 2    60   ~ 0
SCLK
Text Label 10450 2500 2    60   ~ 0
MISO
Text Label 10450 2600 2    60   ~ 0
MOSI
Text Label 10450 2700 2    60   ~ 0
CS1
NoConn ~ 9200 2500
NoConn ~ 9200 2600
$Comp
L MCP3004-RESCUE-controller U4
U 1 1 5648AF08
P 9700 3450
F 0 "U4" H 9500 3000 60  0000 C CNN
F 1 "MCP3004" H 9650 3900 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-14_4.4x5mm_Pitch0.65mm" H 9750 3200 60  0001 C CNN
F 3 "" H 9750 3200 60  0000 C CNN
	1    9700 3450
	1    0    0    -1  
$EndComp
Text Label 10500 3250 2    60   ~ 0
3V_Ref
Text Label 10500 2200 2    60   ~ 0
3V_Ref
$Comp
L C C8
U 1 1 5648B0CB
P 10900 3350
F 0 "C8" H 10925 3450 50  0000 L CNN
F 1 "100nF" H 10925 3250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 10938 3200 30  0001 C CNN
F 3 "" H 10900 3350 60  0000 C CNN
	1    10900 3350
	1    0    0    -1  
$EndComp
Text Label 10450 3450 2    60   ~ 0
SCLK
Text Label 10450 3550 2    60   ~ 0
MISO
Text Label 10450 3650 2    60   ~ 0
MOSI
Text Label 10450 3750 2    60   ~ 0
CS2
NoConn ~ 9200 3550
NoConn ~ 9200 3650
$Comp
L +3.3V #PWR02
U 1 1 5648C498
P 10900 2050
F 0 "#PWR02" H 10900 1900 50  0001 C CNN
F 1 "+3.3V" H 10900 2190 50  0000 C CNN
F 2 "" H 10900 2050 60  0000 C CNN
F 3 "" H 10900 2050 60  0000 C CNN
	1    10900 2050
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR03
U 1 1 5648C557
P 10900 3100
F 0 "#PWR03" H 10900 2950 50  0001 C CNN
F 1 "+3.3V" H 10900 3240 50  0000 C CNN
F 2 "" H 10900 3100 60  0000 C CNN
F 3 "" H 10900 3100 60  0000 C CNN
	1    10900 3100
	1    0    0    -1  
$EndComp
$Comp
L ST3485E U2
U 1 1 5648C8D4
P 5300 6850
F 0 "U2" H 5150 6500 60  0000 C CNN
F 1 "ST3485E" H 5250 7150 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5250 6750 60  0001 C CNN
F 3 "" H 5250 6750 60  0000 C CNN
	1    5300 6850
	1    0    0    -1  
$EndComp
Text Label 4450 6800 0    60   ~ 0
TxEn
Text Label 4450 6700 0    60   ~ 0
Rx
Text Label 4450 7000 0    60   ~ 0
Tx
$Comp
L C C2
U 1 1 5648D281
P 6200 6850
F 0 "C2" H 6225 6950 50  0000 L CNN
F 1 "100nF" H 6225 6750 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6238 6700 30  0001 C CNN
F 3 "" H 6200 6850 60  0000 C CNN
	1    6200 6850
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR04
U 1 1 5648D4C5
P 5800 6650
F 0 "#PWR04" H 5800 6500 50  0001 C CNN
F 1 "+3.3V" H 5800 6790 50  0000 C CNN
F 2 "" H 5800 6650 60  0000 C CNN
F 3 "" H 5800 6650 60  0000 C CNN
	1    5800 6650
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P9
U 1 1 5648D8CB
P 6700 7300
F 0 "P9" H 6700 7500 50  0000 C CNN
F 1 "CRS485" V 6800 7300 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Angled_1x03" H 6700 7300 60  0001 C CNN
F 3 "" H 6700 7300 60  0000 C CNN
	1    6700 7300
	1    0    0    -1  
$EndComp
Text Notes 4900 6400 0    60   ~ 0
RS-485 communication.
$Comp
L MCP3004-RESCUE-controller U5
U 1 1 5648E8CC
P 9700 4500
F 0 "U5" H 9500 4050 60  0000 C CNN
F 1 "MCP3004" H 9650 4950 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-14_4.4x5mm_Pitch0.65mm" H 9750 4250 60  0001 C CNN
F 3 "" H 9750 4250 60  0000 C CNN
	1    9700 4500
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 5648EA40
P 10900 4400
F 0 "C9" H 10925 4500 50  0000 L CNN
F 1 "100nF" H 10925 4300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 10938 4250 30  0001 C CNN
F 3 "" H 10900 4400 60  0000 C CNN
	1    10900 4400
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR05
U 1 1 5648EB21
P 10900 4150
F 0 "#PWR05" H 10900 4000 50  0001 C CNN
F 1 "+3.3V" H 10900 4290 50  0000 C CNN
F 2 "" H 10900 4150 60  0000 C CNN
F 3 "" H 10900 4150 60  0000 C CNN
	1    10900 4150
	1    0    0    -1  
$EndComp
Text Label 10500 4300 2    60   ~ 0
3V_Ref
Text Label 10450 4500 2    60   ~ 0
SCLK
Text Label 10450 4600 2    60   ~ 0
MISO
Text Label 10450 4700 2    60   ~ 0
MOSI
Text Label 10450 4800 2    60   ~ 0
CS3
NoConn ~ 9200 4600
NoConn ~ 9200 4700
$Comp
L LM4040 U7
U 1 1 56D9E6F6
P 9800 1100
F 0 "U7" H 9950 850 60  0000 C CNN
F 1 "LM4040" H 9750 1350 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 9800 950 60  0001 C CNN
F 3 "" H 9800 950 60  0000 C CNN
	1    9800 1100
	1    0    0    -1  
$EndComp
$Comp
L MCP3004-RESCUE-controller U6
U 1 1 56DA0263
P 9700 5550
F 0 "U6" H 9500 5100 60  0000 C CNN
F 1 "MCP3004" H 9650 6000 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-14_4.4x5mm_Pitch0.65mm" H 9750 5300 60  0001 C CNN
F 3 "" H 9750 5300 60  0000 C CNN
	1    9700 5550
	1    0    0    -1  
$EndComp
$Comp
L C C10
U 1 1 56DA026A
P 10900 5450
F 0 "C10" H 10925 5550 50  0000 L CNN
F 1 "100nF" H 10925 5350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 10938 5300 30  0001 C CNN
F 3 "" H 10900 5450 60  0000 C CNN
	1    10900 5450
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR06
U 1 1 56DA0273
P 10900 5200
F 0 "#PWR06" H 10900 5050 50  0001 C CNN
F 1 "+3.3V" H 10900 5340 50  0000 C CNN
F 2 "" H 10900 5200 60  0000 C CNN
F 3 "" H 10900 5200 60  0000 C CNN
	1    10900 5200
	1    0    0    -1  
$EndComp
Text Label 10500 5350 2    60   ~ 0
3V_Ref
Text Label 10450 5550 2    60   ~ 0
SCLK
Text Label 10450 5650 2    60   ~ 0
MISO
Text Label 10450 5750 2    60   ~ 0
MOSI
Text Label 10450 5850 2    60   ~ 0
CS4
NoConn ~ 9200 5650
NoConn ~ 9200 5750
$Comp
L SN74LVC1G139 U1
U 1 1 56DA0DB0
P 2800 6950
F 0 "U1" H 2650 6700 60  0000 C CNN
F 1 "SN74LVC1G139" H 2800 7300 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 2800 6950 60  0001 C CNN
F 3 "" H 2800 6950 60  0000 C CNN
	1    2800 6950
	1    0    0    -1  
$EndComp
Text Label 3400 6750 2    60   ~ 0
CS1
Text Label 3400 6850 2    60   ~ 0
CS2
Text Label 3400 6950 2    60   ~ 0
CS3
Text Label 3400 7050 2    60   ~ 0
CS4
$Comp
L C C1
U 1 1 56DA22FA
P 1950 6900
F 0 "C1" H 1975 7000 50  0000 L CNN
F 1 "100nF" H 1975 6800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1988 6750 30  0001 C CNN
F 3 "" H 1950 6900 60  0000 C CNN
	1    1950 6900
	1    0    0    -1  
$EndComp
$Sheet
S 7750 2000 1200 500 
U 56DB8BBC
F0 "VoltageConditioning" 60
F1 "v_conditioning.sch" 60
F2 "V1" O R 8950 2100 60 
F3 "V2" O R 8950 2200 60 
F4 "V3" O R 8950 2300 60 
F5 "V4" O R 8950 2400 60 
F6 "V1_in" I L 7750 2100 60 
F7 "V2_in" I L 7750 2200 60 
F8 "V3_in" I L 7750 2300 60 
F9 "V4_in" I L 7750 2400 60 
$EndSheet
$Comp
L R R1
U 1 1 56DC8026
P 6200 7300
F 0 "R1" V 6100 7300 50  0000 C CNN
F 1 "110R" V 6200 7300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6130 7300 50  0001 C CNN
F 3 "" H 6200 7300 50  0000 C CNN
	1    6200 7300
	0    1    1    0   
$EndComp
$Sheet
S 7750 2850 1200 950 
U 56DC4973
F0 "CurrentConditioning" 60
F1 "i_conditioning.sch" 60
F2 "I3_in1" I L 7750 3400 60 
F3 "I3_in2" I L 7750 3500 60 
F4 "VRef" I L 7750 2900 60 
F5 "I3" O R 8950 3350 60 
F6 "I4_in1" I L 7750 3600 60 
F7 "I4_in2" I L 7750 3700 60 
F8 "I4" O R 8950 3450 60 
F9 "I1_in1" I L 7750 3000 60 
F10 "I1_in2" I L 7750 3100 60 
F11 "I1" O R 8950 3150 60 
F12 "I2_in1" I L 7750 3200 60 
F13 "I2_in2" I L 7750 3300 60 
F14 "I2" O R 8950 3250 60 
$EndSheet
Text Label 7300 2900 0    60   ~ 0
3V_Ref
$Comp
L C C6
U 1 1 56E5A3D6
P 10650 1250
F 0 "C6" H 10675 1350 50  0000 L CNN
F 1 "1uF" H 10675 1150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 10688 1100 30  0001 C CNN
F 3 "" H 10650 1250 60  0000 C CNN
	1    10650 1250
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P3
U 1 1 56E5C740
P 6550 3100
F 0 "P3" H 6550 3300 50  0000 C CNN
F 1 "CI1" V 6650 3100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 6550 3100 50  0001 C CNN
F 3 "" H 6550 3100 50  0000 C CNN
	1    6550 3100
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X03 P4
U 1 1 56E5EBD6
P 6550 3600
F 0 "P4" H 6550 3800 50  0000 C CNN
F 1 "CI2" V 6650 3600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 6550 3600 50  0001 C CNN
F 3 "" H 6550 3600 50  0000 C CNN
	1    6550 3600
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X03 P5
U 1 1 56E5F0B5
P 6550 4100
F 0 "P5" H 6550 4300 50  0000 C CNN
F 1 "CI3" V 6650 4100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 6550 4100 50  0001 C CNN
F 3 "" H 6550 4100 50  0000 C CNN
	1    6550 4100
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X03 P6
U 1 1 56E5F5A8
P 6550 4600
F 0 "P6" H 6550 4800 50  0000 C CNN
F 1 "CI4" V 6650 4600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 6550 4600 50  0001 C CNN
F 3 "" H 6550 4600 50  0000 C CNN
	1    6550 4600
	-1   0    0    1   
$EndComp
$Sheet
S 7750 4000 1200 900 
U 56E64B53
F0 "TempConditioning" 60
F1 "t_conditioning.sch" 60
F2 "T1_in2" I L 7750 4200 60 
F3 "T1_in1" I L 7750 4100 60 
F4 "T1_out" O R 8950 4200 60 
F5 "T2_in1" I L 7750 4300 60 
F6 "T2_in2" I L 7750 4400 60 
F7 "T2_out" O R 8950 4300 60 
F8 "T3_in2" I L 7750 4600 60 
F9 "T3_in1" I L 7750 4500 60 
F10 "T3_out" O R 8950 4400 60 
F11 "T4_in1" I L 7750 4700 60 
F12 "T4_in2" I L 7750 4800 60 
F13 "T4_out" O R 8950 4500 60 
$EndSheet
$Comp
L CONN_01X02 P7
U 1 1 5727223B
P 6550 5150
F 0 "P7" H 6550 5300 50  0000 C CNN
F 1 "CT1" V 6650 5150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 6550 5150 50  0001 C CNN
F 3 "" H 6550 5150 50  0000 C CNN
	1    6550 5150
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X02 P8
U 1 1 57272400
P 6550 5650
F 0 "P8" H 6550 5800 50  0000 C CNN
F 1 "CT4" V 6650 5650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 6550 5650 50  0001 C CNN
F 3 "" H 6550 5650 50  0000 C CNN
	1    6550 5650
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 5727254B
P 6050 5150
F 0 "P1" H 6050 5300 50  0000 C CNN
F 1 "CT2" V 6150 5150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 6050 5150 50  0001 C CNN
F 3 "" H 6050 5150 50  0000 C CNN
	1    6050 5150
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X02 P2
U 1 1 5727271D
P 6050 5650
F 0 "P2" H 6050 5800 50  0000 C CNN
F 1 "CT3" V 6150 5650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 6050 5650 50  0001 C CNN
F 3 "" H 6050 5650 50  0000 C CNN
	1    6050 5650
	-1   0    0    1   
$EndComp
$Comp
L +3.3V #PWR07
U 1 1 5728AEF0
P 1950 6650
F 0 "#PWR07" H 1950 6500 50  0001 C CNN
F 1 "+3.3V" H 1950 6790 50  0000 C CNN
F 2 "" H 1950 6650 60  0000 C CNN
F 3 "" H 1950 6650 60  0000 C CNN
	1    1950 6650
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X19 P13
U 1 1 5726EDE8
P 1500 5200
F 0 "P13" H 1500 6200 50  0000 C CNN
F 1 "CONN_02X19" V 1500 5200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x19" H 1500 4200 50  0001 C CNN
F 3 "" H 1500 4200 50  0000 C CNN
	1    1500 5200
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X19 P14
U 1 1 5726F475
P 2850 5200
F 0 "P14" H 2850 6200 50  0000 C CNN
F 1 "CONN_02X19" V 2850 5200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x19" H 2850 4200 50  0001 C CNN
F 3 "" H 2850 4200 50  0000 C CNN
	1    2850 5200
	1    0    0    -1  
$EndComp
Text Label 2350 4800 0    60   ~ 0
SCLK
Text Label 2350 4900 0    60   ~ 0
MISO
Text Label 2350 5000 0    60   ~ 0
MOSI
Text Label 2350 6100 0    60   ~ 0
Rx
Text Label 2350 6000 0    60   ~ 0
Tx
Text Label 2350 5900 0    60   ~ 0
TxEn
NoConn ~ 1750 5500
$Comp
L +3.3V #PWR08
U 1 1 5727E905
P 1900 4950
F 0 "#PWR08" H 1900 4800 50  0001 C CNN
F 1 "+3.3V" H 1900 5090 50  0000 C CNN
F 2 "" H 1900 4950 60  0000 C CNN
F 3 "" H 1900 4950 60  0000 C CNN
	1    1900 4950
	1    0    0    -1  
$EndComp
NoConn ~ 2600 4300
NoConn ~ 2600 4400
NoConn ~ 2600 4500
NoConn ~ 2600 4600
NoConn ~ 3100 4300
NoConn ~ 3100 4400
NoConn ~ 3100 4500
NoConn ~ 3100 4600
NoConn ~ 3100 4700
NoConn ~ 3100 4800
NoConn ~ 3100 4900
NoConn ~ 3100 5000
NoConn ~ 3100 5100
NoConn ~ 2600 5100
NoConn ~ 2600 5200
NoConn ~ 2600 5300
NoConn ~ 2600 5400
NoConn ~ 2600 5500
NoConn ~ 2600 5600
NoConn ~ 3100 5900
NoConn ~ 3100 6000
NoConn ~ 3100 6100
NoConn ~ 3100 5700
NoConn ~ 3100 5600
NoConn ~ 3100 5500
NoConn ~ 3100 5400
NoConn ~ 3100 5300
NoConn ~ 1750 4300
NoConn ~ 1750 4400
NoConn ~ 1750 4500
NoConn ~ 1750 4600
NoConn ~ 1750 4700
NoConn ~ 1750 4800
NoConn ~ 1750 4900
NoConn ~ 1250 4300
NoConn ~ 1250 4400
NoConn ~ 1250 4500
NoConn ~ 1250 4600
NoConn ~ 1250 4700
NoConn ~ 1250 4800
NoConn ~ 1250 4900
NoConn ~ 1250 5000
NoConn ~ 1250 5100
NoConn ~ 1250 5200
NoConn ~ 1750 5100
NoConn ~ 1250 5300
NoConn ~ 1250 5400
NoConn ~ 1250 5500
NoConn ~ 1250 5600
NoConn ~ 1250 5700
NoConn ~ 1250 5800
NoConn ~ 1250 5900
NoConn ~ 1250 6000
NoConn ~ 1250 6100
NoConn ~ 1750 5600
NoConn ~ 1750 5700
NoConn ~ 1750 5800
NoConn ~ 1750 5900
NoConn ~ 1750 6000
NoConn ~ 1750 6100
$Comp
L +12V #PWR09
U 1 1 5728DD58
P 2050 5350
F 0 "#PWR09" H 2050 5200 50  0001 C CNN
F 1 "+12V" H 2050 5490 50  0000 C CNN
F 2 "" H 2050 5350 50  0000 C CNN
F 3 "" H 2050 5350 50  0000 C CNN
	1    2050 5350
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X04 P10
U 1 1 572B3758
P 7400 2250
F 0 "P10" H 7400 2500 50  0000 C CNN
F 1 "CV" H 7400 2000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04" H 7400 1050 50  0001 C CNN
F 3 "" H 7400 1050 50  0000 C CNN
	1    7400 2250
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X04 P11
U 1 1 572BEB27
P 8750 5400
F 0 "P11" H 8750 5650 50  0000 C CNN
F 1 "CA" H 8750 5150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04" H 8750 4200 50  0001 C CNN
F 3 "" H 8750 4200 50  0000 C CNN
	1    8750 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 1400 8800 1450
Wire Wire Line
	8800 1450 10650 1450
Wire Wire Line
	9150 1450 9150 1400
Connection ~ 9150 1450
Wire Wire Line
	9150 1100 9150 1050
Wire Wire Line
	8800 1050 9350 1050
Wire Wire Line
	8800 950  8800 1100
Connection ~ 9150 1050
Wire Wire Line
	10300 1050 10950 1050
Wire Wire Line
	10350 1050 10350 1100
Wire Wire Line
	10350 1450 10350 1400
Connection ~ 9800 1450
Connection ~ 10350 1050
Connection ~ 8800 1050
Wire Notes Line
	8650 750  11000 750 
Wire Notes Line
	11000 750  11000 1700
Wire Notes Line
	11000 1700 8650 1700
Wire Notes Line
	8650 1700 8650 750 
Wire Wire Line
	10200 2100 10900 2100
Wire Wire Line
	10900 2050 10900 2150
Wire Wire Line
	10200 2300 10650 2300
Wire Wire Line
	10650 2300 10650 2500
Wire Wire Line
	10650 2500 10900 2500
Wire Wire Line
	10900 2450 10900 2550
Connection ~ 10900 2500
Wire Wire Line
	9200 2700 9100 2700
Wire Wire Line
	9100 2700 9100 5950
Wire Wire Line
	10450 2400 10200 2400
Wire Wire Line
	10200 2500 10450 2500
Wire Wire Line
	10450 2600 10200 2600
Wire Wire Line
	10200 2700 10450 2700
Wire Wire Line
	10200 2200 10500 2200
Wire Wire Line
	9200 3750 9100 3750
Connection ~ 9100 3750
Wire Wire Line
	10200 3250 10500 3250
Wire Wire Line
	10200 3150 10900 3150
Wire Wire Line
	10900 3100 10900 3200
Wire Wire Line
	10900 3500 10900 3600
Wire Wire Line
	10200 3350 10650 3350
Wire Wire Line
	10650 3350 10650 3550
Wire Wire Line
	10650 3550 10900 3550
Connection ~ 10900 3550
Wire Wire Line
	10450 3450 10200 3450
Wire Wire Line
	10200 3550 10450 3550
Wire Wire Line
	10450 3650 10200 3650
Wire Wire Line
	10200 3750 10450 3750
Connection ~ 10900 3150
Connection ~ 10900 2100
Wire Wire Line
	4450 6800 4850 6800
Wire Wire Line
	4850 6900 4750 6900
Wire Wire Line
	4750 6900 4750 6800
Connection ~ 4750 6800
Wire Wire Line
	4850 7000 4450 7000
Wire Wire Line
	4850 6700 4450 6700
Wire Wire Line
	5750 7000 6200 7000
Wire Wire Line
	5800 7000 5800 7450
Wire Wire Line
	5750 6700 6200 6700
Connection ~ 5800 7000
Wire Wire Line
	5800 6650 5800 6700
Connection ~ 5800 6700
Wire Wire Line
	5750 6900 5950 6900
Wire Wire Line
	5750 6800 6000 6800
Wire Wire Line
	5800 7400 6500 7400
Connection ~ 5800 7400
Wire Notes Line
	6900 6250 6900 7700
Wire Notes Line
	6900 7700 4350 7700
Wire Wire Line
	9200 4800 9100 4800
Connection ~ 9100 4800
Wire Wire Line
	10200 4200 10900 4200
Wire Wire Line
	10900 4150 10900 4250
Connection ~ 10900 4200
Wire Wire Line
	10900 4550 10900 4650
Wire Wire Line
	10900 4600 10650 4600
Wire Wire Line
	10650 4600 10650 4400
Wire Wire Line
	10650 4400 10200 4400
Connection ~ 10900 4600
Wire Wire Line
	10200 4300 10500 4300
Wire Wire Line
	10450 4800 10200 4800
Wire Wire Line
	10200 4700 10450 4700
Wire Wire Line
	10450 4600 10200 4600
Wire Wire Line
	10200 4500 10450 4500
Wire Wire Line
	9200 5850 9100 5850
Wire Wire Line
	10200 5250 10900 5250
Wire Wire Line
	10900 5200 10900 5300
Connection ~ 10900 5250
Wire Wire Line
	10900 5600 10900 5700
Wire Wire Line
	10900 5650 10650 5650
Wire Wire Line
	10650 5650 10650 5450
Wire Wire Line
	10650 5450 10200 5450
Connection ~ 10900 5650
Wire Wire Line
	10200 5350 10500 5350
Wire Wire Line
	10450 5850 10200 5850
Wire Wire Line
	10200 5750 10450 5750
Wire Wire Line
	10450 5650 10200 5650
Wire Wire Line
	10200 5550 10450 5550
Wire Wire Line
	3200 6750 3400 6750
Wire Wire Line
	3200 6850 3400 6850
Wire Wire Line
	3200 6950 3400 6950
Wire Wire Line
	3200 7050 3400 7050
Wire Wire Line
	2400 6750 1950 6750
Wire Wire Line
	1950 7050 2400 7050
Wire Wire Line
	1950 7050 1950 7100
Wire Wire Line
	9200 2100 8950 2100
Wire Wire Line
	8950 2200 9200 2200
Wire Wire Line
	9200 2300 8950 2300
Wire Wire Line
	8950 2400 9200 2400
Wire Wire Line
	5950 7300 6050 7300
Wire Wire Line
	6000 6800 6000 7050
Wire Wire Line
	6000 7050 6400 7050
Wire Wire Line
	6400 7050 6400 7300
Connection ~ 6400 7300
Wire Wire Line
	9200 3450 8950 3450
Wire Wire Line
	9200 3350 8950 3350
Wire Wire Line
	8950 3250 9200 3250
Wire Wire Line
	9200 3150 8950 3150
Wire Wire Line
	7750 2900 7300 2900
Wire Wire Line
	10650 1100 10650 1050
Connection ~ 10650 1050
Wire Wire Line
	10650 1450 10650 1400
Connection ~ 10350 1450
Wire Wire Line
	6750 3000 7750 3000
Wire Wire Line
	7750 3100 6750 3100
Wire Wire Line
	6750 3200 6850 3200
Wire Wire Line
	6750 3500 7000 3500
Wire Wire Line
	7000 3500 7000 3200
Wire Wire Line
	7000 3200 7750 3200
Wire Wire Line
	7750 3300 7050 3300
Wire Wire Line
	7050 3300 7050 3600
Wire Wire Line
	7050 3600 6750 3600
Wire Wire Line
	6850 3700 6750 3700
Wire Wire Line
	6750 4000 7100 4000
Wire Wire Line
	7100 4000 7100 3400
Wire Wire Line
	7100 3400 7750 3400
Wire Wire Line
	7750 3500 7150 3500
Wire Wire Line
	7150 3500 7150 4100
Wire Wire Line
	7150 4100 6750 4100
Wire Wire Line
	6850 4200 6750 4200
Wire Wire Line
	6750 4500 7200 4500
Wire Wire Line
	7200 4500 7200 3600
Wire Wire Line
	7200 3600 7750 3600
Wire Wire Line
	7750 3700 7250 3700
Wire Wire Line
	7250 3700 7250 4600
Wire Wire Line
	7250 4600 6750 4600
Wire Wire Line
	6850 4700 6750 4700
Wire Wire Line
	8950 4200 9200 4200
Wire Wire Line
	9200 4300 8950 4300
Wire Wire Line
	8950 4400 9200 4400
Wire Wire Line
	9200 4500 8950 4500
Wire Wire Line
	7750 4100 7300 4100
Wire Wire Line
	7300 4100 7300 5100
Wire Wire Line
	7300 5100 6750 5100
Wire Wire Line
	7750 4200 7350 4200
Wire Wire Line
	7350 4200 7350 5200
Wire Wire Line
	7350 5200 6750 5200
Wire Wire Line
	7750 4300 7400 4300
Wire Wire Line
	7400 4300 7400 5350
Wire Wire Line
	7400 5350 6400 5350
Wire Wire Line
	6400 5350 6400 5100
Wire Wire Line
	6400 5100 6250 5100
Wire Wire Line
	7750 4400 7450 4400
Wire Wire Line
	7450 4400 7450 5400
Wire Wire Line
	7450 5400 6350 5400
Wire Wire Line
	6350 5400 6350 5200
Wire Wire Line
	6350 5200 6250 5200
Wire Wire Line
	7750 4500 7500 4500
Wire Wire Line
	7500 4500 7500 5450
Wire Wire Line
	7500 5450 6350 5450
Wire Wire Line
	6350 5450 6350 5600
Wire Wire Line
	6350 5600 6250 5600
Wire Wire Line
	7750 4600 7550 4600
Wire Wire Line
	7550 4600 7550 5500
Wire Wire Line
	7550 5500 6400 5500
Wire Wire Line
	6400 5500 6400 5700
Wire Wire Line
	6400 5700 6250 5700
Wire Wire Line
	7750 4700 7600 4700
Wire Wire Line
	7600 4700 7600 5600
Wire Wire Line
	7600 5600 6750 5600
Wire Wire Line
	7750 4800 7650 4800
Wire Wire Line
	7650 4800 7650 5700
Wire Wire Line
	7650 5700 6750 5700
Wire Wire Line
	1950 6750 1950 6650
Connection ~ 1950 6750
Connection ~ 9100 5850
Wire Wire Line
	2600 4700 2250 4700
Wire Wire Line
	2250 4700 2250 4750
Wire Wire Line
	3250 5250 3250 5200
Wire Wire Line
	3250 5200 3100 5200
Wire Wire Line
	2600 4800 2350 4800
Wire Wire Line
	2600 4900 2350 4900
Wire Wire Line
	2600 5000 2350 5000
Wire Wire Line
	2600 6100 2350 6100
Wire Wire Line
	2600 6000 2350 6000
Wire Wire Line
	2600 5900 2350 5900
Wire Wire Line
	2200 5700 2600 5700
Wire Wire Line
	2250 5800 2600 5800
Wire Wire Line
	1750 5200 1900 5200
Wire Wire Line
	1900 5200 1900 5950
Wire Wire Line
	1750 5300 1900 5300
Connection ~ 1900 5300
Wire Wire Line
	1900 4950 1900 5000
Wire Wire Line
	1900 5000 1750 5000
Wire Wire Line
	3250 5900 3250 5800
Wire Wire Line
	3250 5800 3100 5800
Wire Wire Line
	2200 5700 2200 6950
Wire Wire Line
	2250 5800 2250 6850
Wire Notes Line
	6900 6250 4350 6250
Wire Notes Line
	4350 6250 4350 7700
Wire Wire Line
	2050 5350 2050 5400
Wire Wire Line
	2050 5400 1750 5400
Connection ~ 1950 7050
Wire Wire Line
	6350 7300 6500 7300
Wire Wire Line
	5950 6900 5950 7300
Wire Wire Line
	5950 7150 6300 7150
Wire Wire Line
	6300 7150 6300 7200
Wire Wire Line
	6300 7200 6500 7200
Connection ~ 5950 7150
Wire Wire Line
	2200 6950 2400 6950
Wire Wire Line
	2250 6850 2400 6850
Wire Wire Line
	7750 2100 7650 2100
Wire Wire Line
	7650 2200 7750 2200
Wire Wire Line
	7750 2300 7650 2300
Wire Wire Line
	7650 2400 7750 2400
Wire Wire Line
	7150 2400 7050 2400
Wire Wire Line
	7050 2100 7050 2450
Wire Wire Line
	7150 2300 7050 2300
Connection ~ 7050 2400
Wire Wire Line
	7150 2200 7050 2200
Connection ~ 7050 2300
Wire Wire Line
	7150 2100 7050 2100
Connection ~ 7050 2200
Wire Wire Line
	9200 5250 9000 5250
Wire Wire Line
	9000 5350 9200 5350
Wire Wire Line
	9200 5450 9000 5450
Wire Wire Line
	9000 5550 9200 5550
Wire Wire Line
	8450 5250 8450 5650
Wire Wire Line
	8450 5550 8500 5550
Wire Wire Line
	8500 5450 8450 5450
Connection ~ 8450 5550
Wire Wire Line
	8500 5350 8450 5350
Connection ~ 8450 5450
Wire Wire Line
	8450 5250 8500 5250
Connection ~ 8450 5350
$Comp
L GND #PWR010
U 1 1 572DBA8C
P 1950 7100
F 0 "#PWR010" H 1950 6850 50  0001 C CNN
F 1 "GND" H 1950 6950 50  0000 C CNN
F 2 "" H 1950 7100 50  0000 C CNN
F 3 "" H 1950 7100 50  0000 C CNN
	1    1950 7100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 572DD586
P 3250 5250
F 0 "#PWR011" H 3250 5000 50  0001 C CNN
F 1 "GND" H 3250 5100 50  0000 C CNN
F 2 "" H 3250 5250 50  0000 C CNN
F 3 "" H 3250 5250 50  0000 C CNN
	1    3250 5250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 572DD6B4
P 3250 5900
F 0 "#PWR012" H 3250 5650 50  0001 C CNN
F 1 "GND" H 3250 5750 50  0000 C CNN
F 2 "" H 3250 5900 50  0000 C CNN
F 3 "" H 3250 5900 50  0000 C CNN
	1    3250 5900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 572DD7E2
P 1900 5950
F 0 "#PWR013" H 1900 5700 50  0001 C CNN
F 1 "GND" H 1900 5800 50  0000 C CNN
F 2 "" H 1900 5950 50  0000 C CNN
F 3 "" H 1900 5950 50  0000 C CNN
	1    1900 5950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 572DD910
P 2250 4750
F 0 "#PWR014" H 2250 4500 50  0001 C CNN
F 1 "GND" H 2250 4600 50  0000 C CNN
F 2 "" H 2250 4750 50  0000 C CNN
F 3 "" H 2250 4750 50  0000 C CNN
	1    2250 4750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 572DEDDD
P 9100 5950
F 0 "#PWR015" H 9100 5700 50  0001 C CNN
F 1 "GND" H 9100 5800 50  0000 C CNN
F 2 "" H 9100 5950 50  0000 C CNN
F 3 "" H 9100 5950 50  0000 C CNN
	1    9100 5950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 572E0B24
P 8450 5650
F 0 "#PWR016" H 8450 5400 50  0001 C CNN
F 1 "GND" H 8450 5500 50  0000 C CNN
F 2 "" H 8450 5650 50  0000 C CNN
F 3 "" H 8450 5650 50  0000 C CNN
	1    8450 5650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 572E0C52
P 10900 5700
F 0 "#PWR017" H 10900 5450 50  0001 C CNN
F 1 "GND" H 10900 5550 50  0000 C CNN
F 2 "" H 10900 5700 50  0000 C CNN
F 3 "" H 10900 5700 50  0000 C CNN
	1    10900 5700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR018
U 1 1 572E1EEE
P 10900 4650
F 0 "#PWR018" H 10900 4400 50  0001 C CNN
F 1 "GND" H 10900 4500 50  0000 C CNN
F 2 "" H 10900 4650 50  0000 C CNN
F 3 "" H 10900 4650 50  0000 C CNN
	1    10900 4650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 572E3497
P 10900 3600
F 0 "#PWR019" H 10900 3350 50  0001 C CNN
F 1 "GND" H 10900 3450 50  0000 C CNN
F 2 "" H 10900 3600 50  0000 C CNN
F 3 "" H 10900 3600 50  0000 C CNN
	1    10900 3600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 572E3F6C
P 10900 2550
F 0 "#PWR020" H 10900 2300 50  0001 C CNN
F 1 "GND" H 10900 2400 50  0000 C CNN
F 2 "" H 10900 2550 50  0000 C CNN
F 3 "" H 10900 2550 50  0000 C CNN
	1    10900 2550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 572E4C49
P 9800 1450
F 0 "#PWR021" H 9800 1200 50  0001 C CNN
F 1 "GND" H 9800 1300 50  0000 C CNN
F 2 "" H 9800 1450 50  0000 C CNN
F 3 "" H 9800 1450 50  0000 C CNN
	1    9800 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 572E6A6E
P 6850 4700
F 0 "#PWR022" H 6850 4450 50  0001 C CNN
F 1 "GND" H 6850 4550 50  0000 C CNN
F 2 "" H 6850 4700 50  0000 C CNN
F 3 "" H 6850 4700 50  0000 C CNN
	1    6850 4700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR023
U 1 1 572E743E
P 6850 4200
F 0 "#PWR023" H 6850 3950 50  0001 C CNN
F 1 "GND" H 6850 4050 50  0000 C CNN
F 2 "" H 6850 4200 50  0000 C CNN
F 3 "" H 6850 4200 50  0000 C CNN
	1    6850 4200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR024
U 1 1 572E756C
P 6850 3700
F 0 "#PWR024" H 6850 3450 50  0001 C CNN
F 1 "GND" H 6850 3550 50  0000 C CNN
F 2 "" H 6850 3700 50  0000 C CNN
F 3 "" H 6850 3700 50  0000 C CNN
	1    6850 3700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR025
U 1 1 572E769A
P 6850 3200
F 0 "#PWR025" H 6850 2950 50  0001 C CNN
F 1 "GND" H 6850 3050 50  0000 C CNN
F 2 "" H 6850 3200 50  0000 C CNN
F 3 "" H 6850 3200 50  0000 C CNN
	1    6850 3200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR026
U 1 1 572E8D9E
P 7050 2450
F 0 "#PWR026" H 7050 2200 50  0001 C CNN
F 1 "GND" H 7050 2300 50  0000 C CNN
F 2 "" H 7050 2450 50  0000 C CNN
F 3 "" H 7050 2450 50  0000 C CNN
	1    7050 2450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR027
U 1 1 572FCAB2
P 5800 7450
F 0 "#PWR027" H 5800 7200 50  0001 C CNN
F 1 "GND" H 5800 7300 50  0000 C CNN
F 2 "" H 5800 7450 50  0000 C CNN
F 3 "" H 5800 7450 50  0000 C CNN
	1    5800 7450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
